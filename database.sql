CREATE DATABASE b57_ecommerce;

USE b57_ecommerce;


-- Table structure for table `categories` 
CREATE TABLE `categories` (
	`category` varchar(255) NOT NULL,
	`name` varchar(255) NOT NULL,
	`id` INT NOT NULL AUTO_INCREMENT,
	PRIMARY KEY (`id`)
);

-- Table structure for table `products`
CREATE TABLE products(
    `id` INT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    `price` FLOAT NOT NULL,
    `image` BLOB NOT NULL,
    `description` VARCHAR(255) NOT NULL,
    `category_id` INT NOT NULL,
    PRIMARY KEY(id)
);

-- Table structure for table `transactions`
CREATE TABLE `transactions` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`transaction_code` INT NOT NULL,
	`purchase_date` TIMESTAMP NOT NULL,
	`total` FLOAT NOT NULL,
	`user_id` INT NOT NULL,
	`payment_mode_id` INT NOT NULL,
	`status_id` INT NOT NULL,
	PRIMARY KEY (`id`)
);

-- Table structure for table `product_transactions`
CREATE TABLE `product_transactions` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`transaction_id` INT NOT NULL,
	`quantity` INT NOT NULL,
	`product_id` INT NOT NULL,
	`price` FLOAT NOT NULL,
	`subtotal` FLOAT NOT NULL,
	PRIMARY KEY (`id`)
);

-- Table structure for table `payment_modes`
CREATE TABLE `payment_modes` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` varchar(255) NOT NULL,
	PRIMARY KEY (`id`)
);


-- Table structure for table `roles`
CREATE TABLE `roles` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` varchar(255) NOT NULL,
	PRIMARY KEY (`id`)
);

-- Table structure for table `statuses`
CREATE TABLE `statuses` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` varchar(255) NOT NULL,
	PRIMARY KEY (`id`)
);


-- Table structure for table `users`
CREATE TABLE `users` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`role_id` INT NOT NULL,
	`password` varchar(255) NOT NULL,
	`email` varchar(255) NOT NULL UNIQUE,
	`firstname` varchar(255) NOT NULL,
	`lastname` varchar(255) NOT NULL,
	PRIMARY KEY (`id`)
);



---------------------------------------------------------------
---Constraints for dumped tables
--------------------------------------------------------------


-- Constraints for table `products`
ALTER TABLE products ADD CONSTRAINT products_fk0 FOREIGN KEY(category_id) REFERENCES categories(id)

-- Constraints for table `product_transactions`
ALTER TABLE product_transactions ADD CONSTRAINT product_transactions_fk0 FOREIGN KEY(transaction_id) REFERENCES transactions(id);
ALTER TABLE product_transactions ADD CONSTRAINT product_transactions_fk1 FOREIGN KEY(product_id) REFERENCES products(id);

-- Constraints for table `transactions`
ALTER TABLE transactions ADD CONSTRAINT transactions_fk0 FOREIGN KEY(user_id) REFERENCES users(id);
ALTER TABLE transactions ADD CONSTRAINT transactions_fk1 FOREIGN KEY(payment_mode_id) REFERENCES payment_modes(id);
ALTER TABLE transactions ADD CONSTRAINT transactions_fk2 FOREIGN KEY(status_id) REFERENCES statuses(id);

-- Constraints for table `users`
ALTER TABLE users ADD CONSTRAINT users_fk0 FOREIGN KEY(role_id) REFERENCES roles(id)
